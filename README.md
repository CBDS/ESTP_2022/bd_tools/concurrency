# Concurrency
Before deciding _how_ to accelerate certain programms/scripts, we first have to understand the nature of the problem.

## Warm up
0. Create a Google account at [google.com](https://google.com) if you don't have one yet
1. Open [Google Colab Notebooks](https://colab.research.google.com)
2. Click Examples
3. Click Overview of Colaboratory Features
4. Explore!
5. Feel free to check out more examples

## Learn about concurrency
You will compare two types of problems, an I/O bound problem, **webscraping**, and a CPU bound problem: **intense calculations**. To tackle these, we compare four approaches:
1. **non-concurrent**, when you don't do anything special
2. **multi-processing**, using multiple CPU cores
3. **threading**, using multiple threads
4. **asyncio**, using asynchroneous requests (only for webscraping)

Skim through the excellent concurrency post at https://realpython.com/python-concurrency/ .

Before you continue and run code, take the time to *understand the code*, at least in broad strokes!

## Concurrency in Colab
1. Download [Concurrency.ipynb](Concurrency.ipynb)
2. Open [Google Colab Notebooks](https://colab.research.google.com)
3. Click upload, upload Concurrency.ipynb
4. Run the cells in order, one by one. We will be (ab)using the notebook to run Linux command line commands.

## Concurrency at home
If possible, repeat the above exercise on your own hardware. Set up a `pipenv` and run the code locally on your machine. On a Linux machine would look like this:

Clone the [realpython](https://realpython.com/) github repository and change to the `concurrency-overview` directory.

    git clone https://github.com/realpython/materials.git
    cd materials/concurrency-overview

Then install pipenv for your user with the Python package manager `pip`. It is bundled with Python since version 3.4.

    pip install --user pipenv

Now we can activate the virtual Python3 environment and install the packages that we need: requests and aiohttp.

    pipenv --three shell
    pipenv install requests aiohttp

Then run the above scripts locally:

    python3 cpu_non_concurrent.py
    python3 [all the other scripts except race_condition.py]

When you are done, exit the pipenv:

    exit

## Answers
If you cannot run the code locally, please give your best guess for the "at home" section.

### General
1. What's the difference between CPU cores and threads?

### Colab

1. How many CPU cores does Colab use?
2. How many threads does Colab use? Note that there could be multiple threads per core!
3. How much faster/slower are the concurrent approaches with respect to non-concurrent?
4. Which approach is targeting which bottleneck?
5. How does it scale with the number of cores/threads?

### At home

1. How many CPU cores does your machine use?
2. How many threads does your machine use? Note that there could be multiple threads per core!
3. How much faster/slower are the concurrent approaches with respect to non-concurrent?
4. How does it scale with the number of cores/threads?
5. Compare these results to Colab.
